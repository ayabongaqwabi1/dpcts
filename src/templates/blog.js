import React from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";
import { Helmet } from "react-helmet";

import { Layout } from "../components/common";

/**
 * Single post view (/:slug)
 *
 * This file renders a single post and loads all the content.
 *
 */
const Post = ({ data, location }) => {
    const markdownRemark = data.markdownRemark;
    const post = markdownRemark.frontmatter;
    console.log("Post data", data);

    return (
        <>
            <Layout>
                <div className="container">
                    <article className="content">
                        {post.featured_img ? (
                            <figure className="post-feature-image">
                                <img src={post.featured_img} alt={post.title} />
                            </figure>
                        ) : null}
                        <section className="post-full-content">
                            <h1 className="content-title">{post.title}</h1>

                            {/* The main post content */}
                            <section
                                className="content-body load-external-scripts"
                                dangerouslySetInnerHTML={{
                                    __html: markdownRemark.html,
                                }}
                            />
                        </section>
                    </article>
                </div>
            </Layout>
        </>
    );
};

Post.propTypes = {
    data: PropTypes.shape({
        ghostPost: PropTypes.shape({
            codeinjection_styles: PropTypes.object,
            title: PropTypes.string.isRequired,
            html: PropTypes.string.isRequired,
            featured_img: PropTypes.string,
        }).isRequired,
    }).isRequired,
    location: PropTypes.object.isRequired,
};

export default Post;

export const pageQuery = graphql`
    query($pathname: String!) {
        markdownRemark(frontmatter: { path: { eq: $pathname } }) {
            html
            frontmatter {
                date(formatString: "MMMM DD, YYYY")
                path
                title
                brief
                featured_img
            }
        }
    }
`;
