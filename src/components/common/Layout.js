import React from "react";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";
import { Link, StaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";

import { Navigation } from ".";
import config from "../../utils/siteConfig";
import PageSeo from "./PageSeo";
// Styles
import "../../styles/app.css";
import Logo from "../../../static/images/dpctslogo2.png";
import Cover from "../../../static/images/cover.jpg";
import "bulma/bulma.sass";
/**
 * Main layout component
 *
 * The Layout component wraps around each page and template.
 * It also provides the header, footer as well as the main
 * styles, and meta data for each page.
 *
 */
const DefaultLayout = ({ data, children, bodyClass, isHome, seo }) => {
    const site = data.site.siteMetadata;
    console.log("SITE", site);
    const twitterUrl = site.twitterusername
        ? `https://twitter.com/bqwabi`
        : null;
    const facebookUrl = site.facebook
        ? `https://www.facebook.com/dopecatsofthecape`
        : null;
    const siteNav = [
        { url: "/", label: "Home" },
        { url: "/downloads", label: "Downloads" },
        { url: "/videos", label: "Videos" },
        { url: "/events", label: "Events" },
        { url: "/interviews", label: "Interviews" },
        { url: "/live", label: "Live Sessions" },
    ];
    const headerClass = isHome ? "site-head" : "site-head";
    return (
        <>
            <PageSeo bodyClass={bodyClass} lang={site.lang} {...seo} />

            <div className="viewport">
                <div className="viewport-top">
                    {/* The main header section on top of the screen */}
                    <header className={headerClass}>
                        <div className="container">
                            <div className="site-mast">
                                <div className="site-mast-left">
                                    <Link to="/">
                                        {Logo ? (
                                            <img
                                                className="site-logo"
                                                src={Logo}
                                                alt={site.title}
                                            />
                                        ) : (
                                            <Img
                                                fixed={
                                                    data.file.childImageSharp
                                                        .fixed
                                                }
                                                alt={site.title}
                                            />
                                        )}
                                    </Link>
                                </div>
                                <div className="site-mast-right">
                                    <a
                                        href={twitterUrl}
                                        className="site-nav-item"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                    >
                                        <img
                                            className="site-nav-icon"
                                            src="/images/icons/twitter.svg"
                                            alt="Twitter"
                                        />
                                    </a>
                                    <a
                                        href={facebookUrl}
                                        className="site-nav-item"
                                        target="_blank"
                                        rel="noopener noreferrer"
                                    >
                                        <img
                                            className="site-nav-icon"
                                            src="/images/icons/facebook.svg"
                                            alt="Facebook"
                                        />
                                    </a>
                                    <a
                                        className="site-nav-item"
                                        href={`https://feedly.com/i/subscription/feed/${config.siteUrl}/rss/`}
                                        target="_blank"
                                        rel="noopener noreferrer"
                                    >
                                        <img
                                            className="site-nav-icon"
                                            src="/images/icons/rss.svg"
                                            alt="RSS Feed"
                                        />
                                    </a>
                                </div>
                            </div>
                            {isHome ? (
                                <div className="site-banner">
                                    <h1 className="site-banner-title">
                                        {site.title}
                                    </h1>
                                    <p className="site-banner-desc">
                                        {site.description}
                                    </p>
                                    <div class="field pt-5">
                                        <div class="control">
                                            <input
                                                class="input is-large"
                                                type="text"
                                                placeholder="Search..."
                                            />
                                        </div>
                                    </div>
                                </div>
                            ) : null}
                        </div>
                        <nav className="site-nav">
                            <div className="site-nav-left">
                                {/* The navigation items as setup in Ghost */}
                                <Navigation
                                    data={siteNav}
                                    navClass="site-nav-item"
                                />
                            </div>
                            <div className="site-nav-right">
                                <Link className="site-nav-button " to="/about">
                                    Upload
                                </Link>
                                <Link className="site-nav-button" to="/about">
                                    About
                                </Link>
                                <Link className="site-nav-button" to="/about">
                                    Contact
                                </Link>
                                <Link className="site-nav-button" to="/about">
                                    Join us
                                </Link>
                            </div>
                        </nav>
                    </header>

                    <main className="site-main">
                        {/* All the main content gets inserted here, index.js, post.js */}
                        {children}
                    </main>
                </div>

                <div className="viewport-bottom">
                    {/* The footer at the very bottom of the screen */}
                    <footer className="site-foot">
                        <div className="site-foot-nav container">
                            <div className="site-foot-nav-left">
                                <Link to="/">{site.title}</Link> © 2019 &mdash;
                                Made with love by
                                <a
                                    className="site-foot-nav-item"
                                    href="https://touch.net.za"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                >
                                    Midas Touch Technologies
                                </a>
                            </div>
                            <div className="site-foot-nav-right">
                                <Navigation
                                    data={siteNav}
                                    navClass="site-foot-nav-item"
                                />
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        </>
    );
};

DefaultLayout.propTypes = {
    children: PropTypes.node.isRequired,
    bodyClass: PropTypes.string,
    isHome: PropTypes.bool,
    data: PropTypes.shape({
        file: PropTypes.object,
        allGhostSettings: PropTypes.object.isRequired,
    }).isRequired,
};

const DefaultLayoutSettingsQuery = (props) => (
    <StaticQuery
        query={graphql`
            query siteSettings {
                site {
                    siteMetadata {
                        title
                        description
                        siteUrl
                        image
                        twitterUsername
                        logo
                    }
                }
            }
        `}
        render={(data) => <DefaultLayout data={data} {...props} />}
    />
);

export default DefaultLayoutSettingsQuery;
