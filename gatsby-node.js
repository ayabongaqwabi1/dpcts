const path = require("path");

exports.createPages = ({ actions, graphql }) => {
    const { createPage } = actions;

    const postTemplate = path.resolve(`src/templates/blog.js`);

    return graphql(`
        {
            allMarkdownRemark {
                edges {
                    node {
                        frontmatter {
                            path
                        }
                    }
                }
            }
        }
    `).then((result) => {
        if (result.errors) {
            return Promise.reject(result.errors);
        }
        console.log(JSON.stringify(result, null, 4));
        const jsonResults = JSON.parse(JSON.stringify(result));
        console.log("JSON RES");
        console.log(jsonResults);
        console.log("DATA", jsonResults.data);
        jsonResults.data.allMarkdownRemark.edges.forEach(({ node }) => {
            console.log("\nmapping nodes");
            console.log(node);

            createPage({
                path: node.frontmatter.path,
                component: postTemplate,
                context: { pathname: node.frontmatter.path }, // additional data can be passed via context
            });
        });
    });
};
